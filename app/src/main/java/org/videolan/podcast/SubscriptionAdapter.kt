package org.videolan.podcast

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import org.videolan.podcast.core.room.entities.PodcastWithEpisodes
import java.util.*

class SubscriptionAdapter(private var mActivity: MainActivity) : RecyclerView.Adapter<SubscriptionAdapter.ViewHolder>(), View.OnClickListener {
    val items = ArrayList<PodcastWithEpisodes>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.subscription_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val podcastEntry = items[position]
        holder.title.text = podcastEntry.podcast.title
        holder.author.text = podcastEntry.podcast.author
        if (podcastEntry.podcast.image != null && podcastEntry.podcast.image!!.isNotEmpty()) {
            Picasso.with(holder.itemView.context).load(podcastEntry.podcast.image).into(holder.image)
        } else {
            holder.image.setImageBitmap(null)
        }
        holder.itemView.setTag(TAG_POSITION, position)
        holder.badge.text = podcastEntry.episodes.size.toString()
        holder.itemView.setOnClickListener(this)

    }

    override fun getItemCount(): Int {
        return items.size
    }


    fun clear() {
        items.clear()
    }

    fun addAll(items: Collection<PodcastWithEpisodes>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onClick(v: View) {
        val position = (v.getTag(TAG_POSITION) as Int).toInt()
        mActivity.removePodcastDialog(items[position].podcast)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var title: TextView = itemView.findViewById<View>(R.id.title) as TextView
        var author: TextView = itemView.findViewById<View>(R.id.author) as TextView
        var image: ImageView = itemView.findViewById<View>(R.id.image) as ImageView
        var badge: TextView = itemView.findViewById(R.id.numberBadge)

    }

    companion object {

        private const val TAG_POSITION = R.id.title
    }
}
