package org.videolan.podcast.storage

import java.io.Closeable
import java.io.IOException

object Util {

    fun closeNicely(closeable: Closeable?) {
        if (closeable == null)
            return
        try {
            closeable.close()
        } catch (e: IOException) {
        }

    }
}
