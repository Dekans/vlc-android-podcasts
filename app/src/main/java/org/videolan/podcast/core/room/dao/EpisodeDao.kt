package org.videolan.podcast.core.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import org.videolan.podcast.core.room.entities.EpisodeEntity

@Dao
interface EpisodeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdate(episodeEntity: EpisodeEntity)

    @Delete
    fun delete(episodeEntity: EpisodeEntity)

    @Query("SELECT * from episode")
    fun getAll(): LiveData<List<EpisodeEntity>>

    @Query("SELECT * from episode")
    fun listAll(): List<EpisodeEntity>

    @Query("SELECT * from episode where link=:link")
    fun get(link: String): EpisodeEntity?

    @Query("SELECT * from episode where podcast_id=:stringId")
    fun getAllByPodcast(stringId: String?) : List<EpisodeEntity>


}