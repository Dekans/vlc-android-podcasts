package org.videolan.podcast.core.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import org.videolan.podcast.core.room.entities.PodcastWithEpisodes

@Dao
interface PodcastsWithEpisodesDao {

    @Query("SELECT * FROM podcast WHERE updated_at IS NOT NULL")
    fun loadPodcastsLive(): LiveData<List<PodcastWithEpisodes>>

    @Query("SELECT * FROM podcast")
    fun loadAllPodcastsWithEpisodes(): List<PodcastWithEpisodes>

    @Query("SELECT * from podcast where link=:link")
    fun loadPodcastWithEpisodes(link:String): PodcastWithEpisodes

    @Query("SELECT * from podcast where link=:link")
    fun findPodcastWithEpisodes(link:String): PodcastWithEpisodes?

}