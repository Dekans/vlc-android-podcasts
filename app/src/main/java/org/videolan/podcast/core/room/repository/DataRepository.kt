package org.videolan.podcast.core.room.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import org.videolan.podcast.core.room.PodcastDatabase
import org.videolan.podcast.core.room.entities.EpisodeEntity
import org.videolan.podcast.core.room.entities.PodcastEntity
import org.videolan.podcast.core.room.entities.PodcastWithEpisodes

class DataRepository private constructor(private val podcastDatabase: PodcastDatabase) {
    private val observablePodcasts: MediatorLiveData<List<PodcastEntity>> = MediatorLiveData()
    private val observableEpisodes: MediatorLiveData<List<EpisodeEntity>> = MediatorLiveData()
    private val observablePodcastsWithEpisodes: MediatorLiveData<List<PodcastWithEpisodes>> = MediatorLiveData()

    /**
     * Get the list of podcasts from the database and get notified when the data changes.
     */
    val podcasts: LiveData<List<PodcastEntity>>
        get() = observablePodcasts

    /**
     * Get the list of episodes from the database and get notified when the data changes.
     */
    val episodes: LiveData<List<EpisodeEntity>>
        get() = observableEpisodes



    val podcastsWithEpisodes: LiveData<List<PodcastWithEpisodes>>
        get() = observablePodcastsWithEpisodes




    init {

        observablePodcasts.addSource(podcastDatabase.podcastDao().getAll()
        ) { podcasts ->
            if (podcastDatabase.databaseCreated.value != null) {
                observablePodcasts.postValue(podcasts)
            }
        }
        observableEpisodes.addSource(podcastDatabase.episodeDao().getAll()
        ) { episodes ->
            if (podcastDatabase.databaseCreated.value != null) {
                observableEpisodes.postValue(episodes)
            }
        }
        observablePodcastsWithEpisodes.addSource(podcastDatabase.podcastsWithEpisodeDao().loadPodcastsLive()
        ) { podcasts ->
            if (podcastDatabase.databaseCreated.value != null) {
                observablePodcastsWithEpisodes.postValue(podcasts.sortedBy { -it.episodes.size })
            }
        }
    }


    companion object {

        private lateinit var instance: DataRepository

        fun getInstance(database: PodcastDatabase): DataRepository {
            if (!this::instance.isInitialized) {
                synchronized(DataRepository::class.java) {
                    instance = DataRepository(database)
                }
            }
            return instance
        }
    }
}
