package org.videolan.podcast.core.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import org.videolan.podcast.core.room.entities.PodcastEntity

@Dao
interface PodcastDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(podcastEntity: PodcastEntity)

    @Update
    fun update(podcast: PodcastEntity)

    @Delete
    fun delete(podcastEntity: PodcastEntity)

    @Query("SELECT * from podcast")
    fun getAll(): LiveData<List<PodcastEntity>>

    @Query("SELECT * from podcast")
    fun listAll(): List<PodcastEntity>

    @Query("SELECT * from podcast where updated_at IS NOT NULL")
    fun listAllValid(): List<PodcastEntity>

    @Query("SELECT * from podcast where link=:link")
    fun get(link: String): PodcastEntity


}