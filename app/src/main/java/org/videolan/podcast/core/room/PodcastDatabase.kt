package org.videolan.podcast.core.room

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import org.videolan.podcast.AppExecutors
import org.videolan.podcast.core.room.dao.EpisodeDao
import org.videolan.podcast.core.room.dao.PodcastDao
import org.videolan.podcast.core.room.dao.PodcastsWithEpisodesDao
import org.videolan.podcast.core.room.entities.EpisodeEntity
import org.videolan.podcast.core.room.entities.PodcastEntity

@Database(entities = [PodcastEntity::class, EpisodeEntity::class], version = 1)
@TypeConverters(AppConverters::class)
abstract class PodcastDatabase : RoomDatabase() {

    abstract fun podcastDao(): PodcastDao
    abstract fun episodeDao(): EpisodeDao
    abstract fun podcastsWithEpisodeDao(): PodcastsWithEpisodesDao

    private val mIsDatabaseCreated = MutableLiveData<Boolean>()

    val databaseCreated: LiveData<Boolean>
        get() = mIsDatabaseCreated


    /**
     * Check whether the database already exists and expose it via [.getDatabaseCreated]
     */
    private fun updateDatabaseCreated(context: Context) {
        if (context.getDatabasePath(DATABASE_NAME).exists()) {
            setDatabaseCreated()
        }
    }


    private fun setDatabaseCreated() {
        mIsDatabaseCreated.postValue(true)
    }






    companion object {


        @VisibleForTesting
        val DATABASE_NAME = "podcasts.db"


        private lateinit var sInstance: PodcastDatabase

        fun getInstance(context: Context, executors: AppExecutors): PodcastDatabase {
            if (!this::sInstance.isInitialized) {
                synchronized(PodcastDatabase::class.java) {
                        sInstance = buildDatabase(context.applicationContext, executors)
                        sInstance.updateDatabaseCreated(context.applicationContext)
                }
            }
            return sInstance
        }



        /**
         * Build the database. [Builder.build] only sets up the database configuration and
         * creates a new instance of the database.
         * The SQLite database is only created when it's accessed for the first time.
         */
        private fun buildDatabase(appContext: Context,
                                  executors: AppExecutors): PodcastDatabase {
            return Room.databaseBuilder(appContext, PodcastDatabase::class.java, DATABASE_NAME)
//                    .addMigrations(MIGRATION_1_2, MIGRATION_2_3)
                    .addCallback(object : RoomDatabase.Callback() {
                        override fun onCreate(db: SupportSQLiteDatabase) {
                            super.onCreate(db)
                            executors.diskIO().execute {
                                // Generate the data for pre-population
                                val database = PodcastDatabase.getInstance(appContext, executors)

                                // notify that the database was created and it's ready to be used
                                database.setDatabaseCreated()
                            }
                        }
                    }).build()
        }




    }
}