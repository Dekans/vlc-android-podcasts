package org.videolan.podcast.core.room.entities

import androidx.room.Embedded
import androidx.room.Relation

class PodcastWithEpisodes {
    @Embedded
    lateinit var podcast: PodcastEntity

    @Relation(parentColumn = "link", entityColumn = "podcast_id")
    var episodes: List<EpisodeEntity> = ArrayList()
}