package org.videolan.podcast.core.room

import androidx.room.TypeConverter
import java.util.*

object AppConverters {
    @TypeConverter
    @JvmStatic
    fun toDate(timestamp: Long?) = timestamp?.let { Date(timestamp) }

    @TypeConverter
    @JvmStatic
    fun toTimestamp(date: Date?) = date?.time
}