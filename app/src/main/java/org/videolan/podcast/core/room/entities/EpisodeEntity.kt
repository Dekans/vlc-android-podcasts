package org.videolan.podcast.core.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "episode", foreignKeys = [ForeignKey(entity = PodcastEntity::class, parentColumns = ["link"] ,childColumns = ["podcast_id"],
    onDelete= ForeignKey.CASCADE)])
data class EpisodeEntity(
        @PrimaryKey
        @ColumnInfo(name = "link")
        var link: String,
        @ColumnInfo(name = "podcast_id", index = true)
        var podcastId: String,
        @ColumnInfo(name = "created_at")
        var createdAt: Date? = null,
        @ColumnInfo(name = "title")
        var title: String? = null,
        @ColumnInfo(name = "description")
        var description: String? = null,
        @ColumnInfo(name = "image")
        var image: String? = null,
        @ColumnInfo(name = "is_audio")
        var isAudio: Boolean = true,
        @ColumnInfo(name = "progress")
        var progress: Float = 0f,
        @ColumnInfo(name = "is_read")
        var isRead: Boolean = false

)