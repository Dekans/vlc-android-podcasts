package org.videolan.podcast.core.update

import android.content.ContentValues
import android.content.Context
import android.util.Log
import android.webkit.URLUtil
import com.rometools.modules.itunes.FeedInformation
import com.rometools.modules.itunes.ITunes
import com.rometools.rome.io.SyndFeedInput
import com.rometools.rome.io.XmlReader
import org.jdom2.Element
import org.videolan.podcast.BuildConfig
import org.videolan.podcast.PodcastApp
import org.videolan.podcast.core.room.entities.EpisodeEntity
import org.videolan.podcast.core.room.entities.PodcastWithEpisodes
import org.videolan.podcast.storage.Util
import org.videolan.vlc.extensions.api.VLCExtensionItem
import java.io.InputStream
import java.net.URL
import java.util.*

object UpdateManager {

    fun updateAll(context: Context) {

        val podcasts = PodcastApp.database.podcastsWithEpisodeDao().loadAllPodcastsWithEpisodes()
        for (podcast in podcasts) {
            updatePodcast(context, podcast)
        }


    }

    fun updatePodcast(context: Context, podcast: PodcastWithEpisodes) {
        var inputStream: InputStream? = null
        try {

            inputStream = URL(podcast.podcast.link).openConnection().getInputStream()
            val input = SyndFeedInput()
            val feed = input.build(XmlReader(inputStream))

            //update podcast values
            var image = feed.image?.url
            var author = feed.author

            for (element in feed.foreignMarkup) {
                if (element.namespace.uri == ITunes.URI && element.name == "image" && element.hasAttributes()) {
                    for (attribute in element.attributes) {
                        if (URLUtil.isValidUrl(attribute.value)) {
                            image = attribute.value
                        }
                    }
                }
                if (element.namespace.uri == ITunes.URI && element.name == "author") {
                    for (attribute in element.content) {
                        author = attribute.value
                    }
                }
            }

            val entryModule = feed.getModule(ITunes.URI)
            if (entryModule != null) {
                val feedInfo = entryModule as FeedInformation
                if (feedInfo.image != null) {
                    image = feedInfo.image.toString()
                }

                if (feedInfo.author != null) {
                    author = feedInfo.author
                }
            }

            podcast.podcast.image = image
            podcast.podcast.author = author
            podcast.podcast.title = feed.title
            podcast.podcast.description = feed.description
            PodcastApp.database.podcastDao().insert(podcast.podcast)




            for (item in feed.entries) {

                val enclosures = item.enclosures
                var link = item.link
                var type = VLCExtensionItem.TYPE_AUDIO
                for (enclosure in enclosures) {
                    if (enclosure.type.startsWith("audio")) {
                        link = enclosure.url
                        type = VLCExtensionItem.TYPE_AUDIO
                        break
                    } else if (enclosure.type.startsWith("video")) {
                        link = enclosure.url
                        type = VLCExtensionItem.TYPE_VIDEO
                        break
                    } else
                        continue
                }

                //format link
//                if (link.contains("?"))
//                    link = link.replace("?", "").split("dest".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0]

                var image: String? = null
                for (element in item.foreignMarkup) {
                    if (element.namespace.uri == ITunes.URI && element.name == "image" && element.hasAttributes()) {
                        for (attribute in element.attributes) {
                            if (URLUtil.isValidUrl(attribute.value)) {
                                image = attribute.value
                            }
                        }
                    }
                }

                if (image == null && podcast.podcast.image != null) {
                    image = podcast.podcast.image
                }

                if (image == null) {
                   image =  (item.foreignMarkup.first { it.name == "group" }.content.first { it is Element && it.name == "thumbnail" } as? Element)?.attributes?.first { it.name == "url" }?.value
                }

                val description = if (item.description != null)
                item.description.value
                else
                    ""

                val episodeEntity = podcast.episodes.filter { it.link == link }.singleOrNull()
                        ?: EpisodeEntity(link, podcast.podcast.link)



                episodeEntity.createdAt = item.publishedDate
                episodeEntity.title = item.title
                episodeEntity.description = description
                episodeEntity.image = image
                episodeEntity.isAudio = type == VLCExtensionItem.TYPE_AUDIO


                PodcastApp.database.episodeDao().insertOrUpdate(episodeEntity)

            }

            podcast.podcast.updatedAt = Date()
            PodcastApp.database.podcastDao().update(podcast.podcast)


        } catch (e: Exception) {
            if (BuildConfig.DEBUG) Log.e(ContentValues.TAG, e.message, e)
        } finally {
            Util.closeNicely(inputStream)
        }
    }

    fun convertStreamToString(`is`: java.io.InputStream): String {
        val s = java.util.Scanner(`is`).useDelimiter("\\A")
        return if (s.hasNext()) s.next() else ""
    }

}