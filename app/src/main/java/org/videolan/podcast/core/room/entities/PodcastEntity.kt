package org.videolan.podcast.core.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "podcast")
data class PodcastEntity(
        @PrimaryKey
        @ColumnInfo(name = "link")
        var link: String,
        @ColumnInfo(name = "title")
        var title: String?,
        @ColumnInfo(name = "description")
        var description: String?,
        @ColumnInfo(name = "image")
        var image: String?,
        @ColumnInfo(name = "author")
        var author: String?,
        @ColumnInfo(name = "updated_at")
        var updatedAt: Date?
)