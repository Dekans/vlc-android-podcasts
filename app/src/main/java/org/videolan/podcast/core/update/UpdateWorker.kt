package org.videolan.podcast.core.update

import android.content.Context
import android.preference.PreferenceManager
import androidx.work.*
import org.videolan.podcast.BuildConfig
import java.util.concurrent.TimeUnit

class UpdateWorker(appContext: Context, workerParams: WorkerParameters) : Worker(appContext, workerParams) {

    override fun doWork(): Result {
        // Do the work here--in this case, upload the images.

        UpdateManager.updateAll(applicationContext)

        // Indicate whether the task finished successfully with the Result
        return Result.success()
    }

    companion object {
        fun launch(context: Context, forceReplace: Boolean = false) {


            val syncDuration = if (BuildConfig.DEBUG) 15 else PreferenceManager.getDefaultSharedPreferences(context)
                    .getLong("sync_frequency", 720)

            val syncUnit = if (BuildConfig.DEBUG) TimeUnit.MINUTES else TimeUnit.HOURS
            val updateWorkRequest = PeriodicWorkRequestBuilder<UpdateWorker>(syncDuration, syncUnit)
                    .build()
            WorkManager.getInstance().enqueueUniquePeriodicWork("podcast_sync", if (forceReplace) ExistingPeriodicWorkPolicy.REPLACE else ExistingPeriodicWorkPolicy.KEEP, updateWorkRequest)
        }
    }
}

