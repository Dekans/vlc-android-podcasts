package org.videolan.podcast.utils

import java.io.*


object FileUtils {

    fun writeStreamToFile(input: InputStream, file: File) {
        try {
            FileOutputStream(file).use { output ->
                val buffer = ByteArray(4 * 1024)
                var read = input.read(buffer)
                while (read  != -1) {
                    output.write(buffer, 0, read)
                    read = input.read(buffer)
                }
                output.flush()
            }
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                input.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }
}